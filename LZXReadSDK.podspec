Pod::Spec.new do |s|
s.name = "LZXReadSDK"
s.version = "2.2.0"
s.license = { :type => "MIT", :file => "LICENSE" }
s.platform = :ios, "8.0"
s.summary = "LZXReadSDK"
s.homepage = "https://gitee.com/xiao_can/LZXReadSDK"
s.authors = { "xiaocan" => "1217272889@qq.com" }
s.source = { :git => "https://gitee.com/xiao_can/LZXReadSDK.git", :tag => "#{s.version}" }
s.requires_arc = true
s.source_files = "LZXReadSDK/*.{h,m}"
s.vendored_libraries = "LZXReadSDK/*.a"
s.public_header_files = "LZXReadSDK/*.h"
s.resources = "LZXReadSDK/*.{bundle,plist,xcdatamodeld,json}"
s.frameworks = "UIKit","Foundation","Security"
s.libraries  = "xml2","z"
s.dependency "DTCoreText"
s.dependency "ZipArchive"
s.dependency "SDWebImage"
s.dependency "SDWebImage/GIF"
s.dependency "FLAnimatedImage"
s.dependency "MJRefresh"
s.dependency "Masonry"
s.dependency "AFNetworking"
s.dependency "SBJson5"
s.dependency "ProgressHUD"
s.dependency "ZLPhotoBrowser"
s.dependency "TOCropViewController"
s.dependency "FMDB","~> 2.7.5"
s.dependency "Protobuf","~> 3.6.1"
s.dependency 'JXCategoryView'
end

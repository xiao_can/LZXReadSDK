//
//  LZXReaderSDKManager.h
//  LZX_Reader
//
//  Created by xiao can on 2018/11/5.
//  Copyright © 2018 xiaocan. All rights reserved.
//
//  sdk verison 2.2.0
//

#import <Foundation/Foundation.h>
#import <StoreKit/StoreKit.h>

@protocol LZXReadSDKDelegate;

NS_ASSUME_NONNULL_BEGIN

UIKIT_EXTERN NSString   *const LZXReadSDKShouldLoginNotification;//通知宿主需要登录
UIKIT_EXTERN NSString   *const LZXReadSDKNotificationControllerKey;//登录通知里面获取触发登录操作的Controller

/** SDK manager */
@interface LZXReaderSDKManager : NSObject

//使用前先注册设备
+ (void)initWithAppKey:(nonnull NSString *)apppKey channel:(nonnull NSString *)channel openLog:(BOOL)openLog;

//使用测试环境,默认为NO,上线时必须设置为NO
+ (void)openTestEnvironment:(BOOL)test;


//更新用户信息
+ (void)updateUserInfo:(nullable NSDictionary *)userInfo animated:(BOOL)animated;

//开启、关闭LOG输出,默认关闭
+ (void)openLog:(BOOL)log;

/** 获取阅读导航控制器 tab在底部 tab：-1或其它、维持上次样式，0、书架，1、书城，2、分类，3、我的*/
+ (nullable UINavigationController *)lzxReaderNavigationViewControllerMoveToTab:(NSInteger)tab;


/** 获取阅读导航控制器 tab 在顶部 tab：-1或其它、维持上次样式，0、书架，1、书城，2、分类，3、我的*/
+ (nullable UINavigationController *)lzxTabTopViewControllerMoveToTab:(NSInteger)tab;

/** 获取小说详情控制器,novelId：资源ID，sourceType：暂时传 @"-1" */
+ (nullable UIViewController *)getNovelDetailController:(NSString *)novelId sourceType:(NSString *)sourceType;


/** 获取书城列表view ,baseController必传，否则内部点击事件无法跳转*/
+ (nullable UIView *)getBookstoresView:(nonnull UIViewController *)baseController;


/** 内购-宿主APP支付状态 */
+ (void)purchasesStatusChanged:(NSString *)message purchasesSuccess:(BOOL)success;


/** 设置SDK代理 */
+ (void)setLZXReadSDKDelegate:(id<LZXReadSDKDelegate>)delegate;

@end




@protocol LZXReadSDKDelegate <NSObject>

@optional


/*
 * @param productID 内购产品ID
 * @param product   内购产品SKProduct
 * @param purchase  SDK生成的充值订单
 *
 * 如充走自己APP充值界面进行充值，以上传值可能为nil
 *
 */

- (void)lzxReadSDKRechargeProductID:(nullable NSString *)productID purchaseInfo:(nullable NSString *)purchase;

- (void)lzxReadSDKSKProduct:(nullable SKProduct *)product purchaseInfo:(nullable NSString *)purchase;

/**
 * 获取当前用户余额
 */
- (NSString *)userCurrentBalance;

@end

NS_ASSUME_NONNULL_END
